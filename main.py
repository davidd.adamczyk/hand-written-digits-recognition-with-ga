from typing import List
import time
import numpy as np
import random
from numba import vectorize, cuda, float32, guvectorize, jit
import pickle
from os import path


class Siec:
  dtype = np.float32
  def __init__(self, numoflayers,_layers, step=0, child=False):
    self.Connections = []
    if not(step==0):
      self.step = step
    else:
      self.step = 0.0001
#    if path.isfile('cache.bin'):
#      with open('cache.bin', 'rb') as _file:
#        content = pickle.load(_file)
#        self.exampleInputs = content[0]
#        self.exampleOutputs = content[1]
#        self.numberOfExamples = content[2]
#        self.exampleInputst = content[3]
#        self.exampleOutputst = content[4]
#        self.numberOfExamplest = content[5]
    if not(child) :
      for i in range(0, numoflayers - 1):
        connectionMatrix = np.empty(shape=(_layers[i + 1], _layers[i]), dtype=self.dtype)
        #randomRange = .2  # 1.
        seedt = int(time.time())  %  100
        np.random.seed(seed=seedt)
        for m in range(0, _layers[i + 1]):
          for n in range(0, _layers[i]):
            connectionMatrix[m][n] = np.random.random_sample() #* randomRange * 2. - randomRange
        self.Connections.append(connectionMatrix)
      #print(self.Layers)
    else:
      self.Connections = []

  @vectorize(['float32(float32, float32)'], target='cpu')
  def add(a, b):
    a = a + b
    return a

  @vectorize(['float32(float32, float32)'], target='cpu')
  def sub(a, b):
    a = a - b
    return a

  @guvectorize([(float32[:, :], float32[:, :], float32[:, :])], '(m,l),(l,n)->(m,n)', target='cpu')
  def matmul_gu(A, B, out):
    """Perform square matrix multiplication of out = A * B
    """
    for i in range(out.shape[0]):
      for j in range(out.shape[1]):
        tmp = 0.
        for k in range(A.shape[1]):
          tmp += A[i, k] * B[k, j]
        out[i, j] = tmp

    #@vectorize(['float32(float32)'], target='cpu')
  #  def _neuronF(x):
  #    x = 1 / (1 + float32(np.e) ** -x)
  #    return x

    #@vectorize(['float32(float32)'], target='cpu')
  #  def _dneuronF(x):
  #    x = 1 / (1 + float32(np.e) ** -x)
  #    x = x * (1 - x)
  #    return x

  def cacheData(self):
    with open('cache.bin', 'wb') as _file:
      content = [
        self.exampleInputs,
        self.exampleOutputs,
        self.numberOfExamples,
        self.exampleInputst,
        self.exampleOutputst,
        self.numberOfExamplest]
      pickle.dump(content, _file)

  def setInput(self,Layers):
    lastLayerIndex = len(Layers) - 1
    for i in range(0, Layers[lastLayerIndex].size):
      Layers[lastLayerIndex][0, i] = 0.5

  def setInputOutput(self, number,Layers): #wczytuje input dla sieci i pobiera "idealny"(wzorcowy) wynik
    lastLayerIndex = len(Layers) - 1
    Layers[lastLayerIndex] = self.exampleInputs[number] #wrzucanie danych wejściowych na ~700 weerzchołków warstwy pierwszej
    return self.exampleOutputs[number]  #zwraca idealny

  def flow(self,Layers): #wylicza wynik sieci
    for i in range(len(self.Connections) - 1, -1, -1):
      self.matmul_gu(Layers[i + 1], self.Connections[i], Layers[i])
      #self.Layers[i] = self._neuronF(self.Layers[i])

  def fintnes(self,Layers,desiredOutput):
    suma = 0
    for i in range(0,len(Layers[0][0])):
      suma += Layers[0][0][i]
    wynikIndex = 0
    for i in range(0,len(desiredOutput)):
      if desiredOutput[i] != 0:
        wynikIndex = i
        break
    wynik = Layers[0][0][wynikIndex]
    suma -= wynik
    if suma == 0:
      suma = 0.001
    wynik = wynik / suma
    return wynik


class AI:
  #tworzymy N zwierzaków
  populacja = [] #atm
  #Jeden zwierzak na start ma:
  #tablice wierzchołków
  #tablice połączeń
  #
  e = np.float32(np.e)
  #zwierzak:
  Layers = []
  #
  index = 0
  dtype = np.float32
  target = 'cpu'  # cuda, parallel, cpu
  saveName = 'neurons.bin'
  step = 0.000001
  maxn = 0

  def __init__(self, _layers, step=0.):
    self.maxn = 10  # atm
    if not step == 0.:
      self.step = step
    self.layers = _layers
    if path.isfile('cache.bin'):
      with open('cache.bin', 'rb') as _file:
        content = pickle.load(_file)
        self.exampleInputs = content[0]
        self.exampleOutputs = content[1]
        self.numberOfExamples = content[2]
        self.exampleInputst = content[3]
        self.exampleOutputst = content[4]
        self.numberOfExamplest = content[5]
    # część wspolna:
    for i in range(0, len(_layers)):
      newVec = np.empty(shape=(1, _layers[i]), dtype=self.dtype) #empty layres?
      self.Layers.append(newVec)
    # inicjalizacja zwierzyny:
    for zwierzak in range(0, self.maxn):  # atm
      self.populacja.append(Siec(len(self.Layers),_layers, step))  # atm
    # for i in range(0, len(self.populacja[zwierzak].Layers) - 1):
     #   connectionMatrix = np.empty(shape=(self.populacja[zwierzak].Layers[i + 1].size, self.populacja[zwierzak].Layers[i].size), dtype=self.dtype)
     #   randomRange = .2  # 1.
     #   np.random.seed(seed=2)
     #   for m in range(0, self.populacja[zwierzak].Layers[i + 1].size):
     #     for n in range(0, self.populacja[zwierzak].Layers[i].size):
     #       connectionMatrix[m][n] = np.random.random_sample() * randomRange * 2. - randomRange
     #   self.populacja[zwierzak].Connections.append(connectionMatrix)
      #print(self.populacja[zwierzak].Layers)

  #@vectorize(['float32(float32, float32)'], target=target)
  def add(a, b):
    a = a + b
    return a
    # @vectorize(['float32(float32, float32)'], target='cpu')

  def sub(a, b):
    a = a - b
    return a

  @guvectorize([(float32[:, :], float32[:, :], float32[:, :])], '(m,l),(l,n)->(m,n)', target=target)
  def matmul_gu(A, B, out):
    """Perform square matrix multiplication of out = A * B
    """
    for i in range(out.shape[0]):
      for j in range(out.shape[1]):
        tmp = 0.
        for k in range(A.shape[1]):
          tmp += A[i, k] * B[k, j]
        out[i, j] = tmp

  @guvectorize([(float32[:, :], float32[:, :], float32, float32[:, :], float32[:, :])], '(l,m),(l,n),(),(m,n)->(m,n)',
               target=target)
  def modify_connections(errorDelta, backlayerResults, delta, connections, out):
    """Perform square matrix multiplication of out = transposed errorDelta * backlayerResults
    """
    for i in range(out.shape[0]):
      for j in range(out.shape[1]):
        out[i, j] = (errorDelta[0, i] * backlayerResults[0, j]) * delta + connections[i, j]
    # out=connections+out

  def cacheData(self):
    with open('cache.bin', 'wb') as _file:
      content = [
        self.exampleInputs,
        self.exampleOutputs,
        self.numberOfExamples,
        self.exampleInputst,
        self.exampleOutputst,
        self.numberOfExamplest]
      pickle.dump(content, _file)

  def setInput(self):
    lastLayerIndex = len(self.Layers) - 1
    for i in range(0, self.Layers[lastLayerIndex].size):
      self.Layers[lastLayerIndex][0, i] = 0.5

  def setInputOutputt(self, number):
    lastLayerIndex = len(self.Layers) - 1
    self.Layers[lastLayerIndex] = self.exampleInputst[number]
    return self.exampleOutputst[number]

  def setInputOutput(self, number): #wczytuje input dla sieci i pobiera "idealny"(wzorcowy) wynik
    lastLayerIndex = len(self.Layers) - 1
    self.Layers[lastLayerIndex] = self.exampleInputs[number] #wrzucanie danych wejściowych na ~700 weerzchołków warstwy pierwszej
    return self.exampleOutputs[number][0]  #zwraca idealny

  #def flow(self): #wylicza wynik sieci
  #  for i in range(len(self.Connections) - 1, -1, -1):
  #    self.matmul_gu(self.Layers[i + 1], self.Connections[i], self.Layers[i])
  #    self.Layers[i] = self._neuronF(self.Layers[i])

  def teach(self): #zmodyfikować
    #setup:
    desiredOutput = np.zeros(self.layers[0], dtype=self.dtype)
    #errorVectors = []
    #for vec in self.Layers:
    #  errorVectors.append(np.copy(vec))
    j = 0
   # ConnChange = []
   # for _i in range(0, len(self.Connections)):
   #   ConnChange.append(np.zeros(shape=self.Connections[_i].shape, dtype=self.dtype)) #niepotrzebna?
    bestPerformance = 0
    #true kod:

    while True:
     # for _i in range(0, len(self.Connections)):
     #   ConnChange[_i] *= 0 #niepotrzebna?
     wyniki = np.zeros(self.maxn, dtype=self.dtype)  ## wyzerować trzeba
     indexes = np.random.permutation(self.numberOfExamples) #fanaberia
     for _o in range(0, self.numberOfExamples): #po każdym przykładzie 'o' z treningowego seta epoka
        o = indexes[_o]
        for zwierzak in range(0, self.maxn):  # atm
          desiredOutput = self.setInputOutput(o) #wczytuje jaki chciałbym żeby był wynik i wczytuje input do AI obiektu
          self.populacja[zwierzak].flow(self.Layers) #wyliczenie co zwraca masza sieć
          wyniki[zwierzak] += self.populacja[zwierzak].fintnes(self.Layers,desiredOutput)
          #j += 1 #wypisanie:
          #if (j >= 1 and j <= 10):
          #  for k in range(10):
          #    if self.populacja[zwierzak].exampleOutputs[o][0, k] > 0.5:
          #      break
          #  print(str(self.Layers[0]) + "      example number " + str(o) + "   " + str(k))
          #if j >= 70000:
          #  j = 0
          #  self.populacja[zwierzak].save()
          # wyliczanie zmiany fitnesu:
          # ee bierzemy największą wartość z ostatniego layera
          #max = 0 ##rzeba policzyć
          # sprawdzamy czy jej index jest zgodny z desired output
          # if( max == desired output
          # możemy zliczać porcent poprawnych trafień
          #wyniki[zwierzak] += self.leyers[0] # placeholder
        #rozmnarzanie:
        nowapopulacja: List[Siec] = []
        for i in range(0,self.maxn):
          #wiadomo mie mogą być losowe: -> trzeba zrobić wersje ruletki (funkcyjke) pythonową
          tabindex = self.select_for_crossover(wyniki, self.populacja)
          index1 = tabindex[0]
          index2 = tabindex[1]
          #index2 = np.random.permutation(len(wyniki) -1)  # test
          #dzieciakL = self.populacja[0].Layers + self.populacja[1].Layers
          nowapopulacja.append(Siec(self.layers, self.step,child=True))
          for j in range(0, len(index1.Connections)):
            nowapopulacja[i].Connections.append((index1.Connections[j] + index2.Connections[j] )/2)
          #nowapopulacja[len(nowapopulacja) - 1].Connections = dzieciakC
        self.populacja = nowapopulacja
        self.mutate()
        #następna epoka:
     #print(self.step)
     # performance = self.test() #na secie tstowym

  def save(self):
    with open(self.saveName, 'wb') as save_file:
      pickle.dump(self.Connections, save_file)

  def getConnectionsFromFile(self):
    with open(self.saveName, 'rb') as read_file:
      self.Connections = pickle.load(read_file)
      self.Layers = []
      i = 0
      for i in range(len(self.Connections)):
        self.Layers.append(np.zeros((1, self.Connections[i].shape[1]), dtype=self.dtype))
      self.Layers.append(np.zeros((1, self.Connections[i].shape[0]), dtype=self.dtype))

  numberOfExamples = 0
  exampleInputs = []
  exampleOutputs = []
  numberOfExamplest = 0
  exampleInputst = []
  exampleOutputst = []

  def readExamplesFile(self, images='train-images.idx3-ubyte', labels='train-labels.idx1-ubyte'):
    if self.numberOfExamples > 0:
      return
    with open(images, 'rb') as examples_file, open(labels, 'rb') as labels_file:
      order = 'big'
      size = 28
      int.from_bytes(examples_file.read(4), order)
      self.numberOfExamples = int.from_bytes(examples_file.read(4), order)
      print("Number of examples: " + str(self.numberOfExamples))
      print("size: " + str(int.from_bytes(examples_file.read(4), order)) + "x" + str(
        int.from_bytes(examples_file.read(4), order)))
      # self.numberOfExamples=10000
      print("reading examples...")
      for i in range(self.numberOfExamples):
        self.exampleInputs.append(np.empty(shape=(1, size * size), dtype=self.dtype))
        for j in range(size * size):
          self.exampleInputs[i][0, j] = int.from_bytes(examples_file.read(1), order) / 255
      # print(self.exampleInputs[0])
      st = ''
      for j in range(size * size):
        if j % size == 0:
          st += '\n'
        if self.exampleInputs[0][0, j] == 0.:
          st += '_'
        else:
          st += 'O'
      print(st)
      int.from_bytes(labels_file.read(8), order)
      print("reading labels...")
      for i in range(self.numberOfExamples):
        self.exampleOutputs.append(np.zeros(shape=(1, 10), dtype=self.dtype))
        self.exampleOutputs[i][0, int.from_bytes(labels_file.read(1), order)] = 1.
      print("finished reading labels")

  def treadExamplesFile(self, images='train-images.idx3-ubyte', labels='train-labels.idx1-ubyte'):
    if self.numberOfExamplest > 0:
      return
    with open(images, 'rb') as examples_file, open(labels, 'rb') as labels_file:
      order = 'big'
      size = 28
      int.from_bytes(examples_file.read(4), order)
      self.numberOfExamplest = int.from_bytes(examples_file.read(4), order)
      print("Number of examples: " + str(self.numberOfExamplest))
      print("size: " + str(int.from_bytes(examples_file.read(4), order)) + "x" + str(
        int.from_bytes(examples_file.read(4), order)))
      print("reading examples...")
      for i in range(self.numberOfExamplest):
        self.exampleInputst.append(np.empty(shape=(1, size * size), dtype=self.dtype))
        for j in range(size * size):
          self.exampleInputst[i][0, j] = int.from_bytes(examples_file.read(1), order) / 255
      st = ''
      for j in range(size * size):
        if j % size == 0:
          st += '\n'
        if self.exampleInputst[0][0, j] == 0.:
          st += '_'
        else:
          st += 'O'
      print(st)
      int.from_bytes(labels_file.read(8), order)
      print("reading labels...")
      for i in range(self.numberOfExamplest):
        self.exampleOutputst.append(np.zeros(shape=(1, 10), dtype=self.dtype))
        self.exampleOutputst[i][0, int.from_bytes(labels_file.read(1), order)] = 1.
      print("finished reading labels")

  def test(self): #testowanie na secie testowym
    good = 0
    all = 0
    for i in range(self.numberOfExamplest):
      output = 0
      for output in range(10):
        if self.exampleOutputst[i][0, output] > 0.9:
          break
      self.setInputOutputt(i)
      self.flow()
      maxValue = self.Layers[0][0, 0]
      maxInd = 0
      for k in range(1, 10):
        if self.Layers[0][0, k] > maxValue:
          maxValue = self.Layers[0][0, k]
          maxInd = k
      all += 1
      if maxInd == output:
        good += 1
    print("Accurancy " + str(good / all))
    return good / all
  def ask(self,vec):
    if vec.shape==self.Layers[len(self.Layers)-1].shape:
      self.Layers[len(self.Layers) - 1]=vec
      self.flow()
      sum=np.sum(self.Layers[0])
      self.Layers[0]=self.Layers[0]/sum
      #max=self.Layers[0]
      maxIndex=0
      for i in range(10):
        if self.Layers[0][0,i]>self.Layers[0][0,maxIndex]:
          maxIndex=i
      return maxIndex,np.copy(self.Layers[0])

  def mutate(self):
    #select for mutation:
    luckyOneindex = np.random.randint(0,self.maxn)
    for c in range(0,len(self.populacja[luckyOneindex].Connections)):
      mutationTimes = np.random.randint(1,5) #stała do ustalenia
      #mutuj
      for i in range(0,mutationTimes):
        randIndexX = np.random.randint(0,self.populacja[luckyOneindex].Connections[c].shape[0])
        randIndexY = np.random.randint(0,self.populacja[luckyOneindex].Connections[c].shape[1])
        randLiczba=random.random() #liczba <0,1)
        self.populacja[luckyOneindex].Connections[c][randIndexX][randIndexY] = randLiczba

  def select_for_crossover(self, wyniki, populacja):
    """"Select individuals for crossover based on fitness."""
    # sort fitness tuples
    population_fitness = wyniki
    fitness_sum = float(sum(population_fitness))
    relative_fitness = [f / fitness_sum for f in population_fitness]
    probabilities = [sum(relative_fitness[:i + 1])
                     for i in range(len(relative_fitness))]
    chosen = []  # rodzice do zwrocenia
    number = 2  # ilosc osobnikow do wybrania
    for n in range(number):
      r = random.random()
      for (i, individual) in enumerate(populacja):
        if r <= probabilities[i]:
          chosen.append(individual)
          break
    return chosen



##########################################################
#######################  Main  ###########################
##########################################################

if __name__ == '__main__':
  #trzeba zrobic tablice obiektów AI? tak ze sto
  AI = AI([10,784],0.0001)#784=28px*28px
    #lepiej by było stworzyć nową klasę Zwierzaka/Sieci


#teaching

  AI.treadExamplesFile('t10k-images.idx3-ubyte', 't10k-labels.idx1-ubyte')
  AI.readExamplesFile()
  AI.cacheData()
  AI.teach()





  AI.getConnectionsFromFile()
  #AI.treadExamplesFile('t10k-images.idx3-ubyte', 't10k-labels.idx1-ubyte') #plik testowy
  #AI.test() # set testowy

  #wczytywanie własnych obrazków:
  from PIL import Image
  with Image.open("img.png",'r') as image:
    b = list(image.getdata())
    #b = bytearray(f)
    vec=np.zeros((1,28*28),AI.dtype)
    for i in range(0,28*28):
      vec[0,i]= 1.-(b[i][0]+b[i][1]+b[i][2])/3/255
    #print(vec)
    s=""
    for i in range(28*28):
      if i%28 == 0:
        s+='\n'
      if vec[0,i]>0.5:
        s+='OO'
      else:
        s+='--'
    print(s)

    answer,percentage=AI.ask(vec)

    for i in range(percentage.shape[1]):
      print(str(i)+":  "+str(percentage[0,i]*100)+"%")
    print("Answer: " + str(answer) + " (" + str(percentage[0, answer]*100) + "%)")






